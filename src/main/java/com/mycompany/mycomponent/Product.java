/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProducList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Coco", 45, "Coco.jpg"));
        list.add(new Product(2, "Cappuccino", 40, "Cappuccino.jpg"));
        list.add(new Product(3, "Mocha", 40, "Mocha.jpg"));
        list.add(new Product(4, "Espresso", 40, "Espresso.jpg"));
        list.add(new Product(5, "Latte", 40, "Latte.jpg"));
        list.add(new Product(6, "Americano", 40, "Americano.jpg"));
        list.add(new Product(7, "Lemon Tea", 40, "LemonTea.jpg"));
        list.add(new Product(8, "Green Tea", 50, "GreenTea.jpg"));
        list.add(new Product(9, "Pink Milk", 40, "PinkMilk.jpg"));
        list.add(new Product(10,"Hot Milk", 40, "HotMilk.jpg"));
        return list;
    }
}
